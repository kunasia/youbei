package jobs

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"time"

	md "youbei/models"
	zipz "youbei/utils/zip"

	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
)

func SshJobs(id string) func() error {
	return func() error {
		if err := SshBackup(id, false); err != nil {
			fmt.Println(err.Error())
		}
		return nil
	}
}

func SshBackup(id string, bol bool) error {
	return execSshBackup(id)
}

func execSshBackup(id string) error {
	sshtask := md.SshTask{}
	bol, err := md.Localdb().ID(id).Get(&sshtask)
	if err != nil {
		return err
	}
	if !bol {
		return errors.New("false")
	}

	sc := new(SshConfig)
	sc.Addr = sshtask.Host
	sc.Port = sshtask.SshPort
	sc.User = sshtask.SshUser
	sc.Password = sshtask.SshPwd
	if err := sc.sshClient(); err != nil {
		return err
	}
	defer sc.Session.Close()
	nowtime := time.Now().Format("2006-01-02_15-04-05")
	dist := sshtask.DbHost + "_mysql_" + sshtask.DbName + "_" + sshtask.Char + "_" + nowtime + ".sql"
	distzip := sshtask.DbHost + "_mysql_" + sshtask.DbName + "_" + sshtask.Char + "_" + nowtime + ".zip"
	srcFile := "/tmp/" + dist
	ditFile := sshtask.SavePath + "/" + dist
	ditzipFile := sshtask.SavePath + "/" + distzip
	cmd := "mysqldump -h" + sshtask.DbHost + " -P" + sshtask.DbPort + " -u" + sshtask.DbUser + " -p" + sshtask.DbPwd + " " + sshtask.DbName + " > " + srcFile
	if err := sc.Run(cmd); err != nil {
		return err
	}
	if sshtask.SavePath != "" {
		if sftpClient, err := sftp.NewClient(sc.Client); err != nil {
			return err
		} else {
			sc.SftpClient = sftpClient
		}
		defer sc.SftpClient.Close()
		srcFile, _ := sc.SftpClient.Open(srcFile)
		dstFile, _ := os.Create(ditFile)
		defer func() {
			srcFile.Close()
			dstFile.Close()
		}()
		if _, err := srcFile.WriteTo(dstFile); err != nil {
			return errors.New("error occurred == " + err.Error())
		} else {
			srcFile.Close()
			dstFile.Close()
			sc.Session.Close()
			sc.Client.Close()
			sc.SftpClient.Close()
		}
		if err := zipz.Zip(ditFile, ditzipFile, sshtask.Zippwd); err != nil {
			return err
		}
		os.Remove(ditFile)
		rs, err := md.TaskFindRemote(sshtask.ID)
		if err != nil {
			return err
		}
		for _, v := range rs {
			err := TestRemote(ditzipFile, v)
			if err != nil {
				return err
			}

		}
	}

	return nil
}

type SshConfig struct {
	Addr       string       // SSH服务器地址
	Port       string       // SSH服务器端口
	User       string       // SSH用户名
	Password   string       // SSH密码
	PrivateKey string       // SSH私钥文件路径
	Session    *ssh.Session // SSH会话
	Client     *ssh.Client  // SSH客户端
	SftpClient *sftp.Client // SFTP客户端
}

// sshClient 建立与SSH服务器的连接
func (c *SshConfig) sshClient() error {
	var err error
	var auth []ssh.AuthMethod // 存储身份验证方式的切片

	// 检查是否设置了PrivateKey字段。如果设置了，使用基于密钥的身份验证。
	if c.PrivateKey != "" {
		key, err := ioutil.ReadFile(c.PrivateKey) // 读取私钥文件
		if err != nil {
			return err
		}
		signer, err := ssh.ParsePrivateKey(key) // 解析私钥文件
		if err != nil {
			return err
		}
		auth = append(auth, ssh.PublicKeys(signer)) // 将解析的私钥添加到auth切片中
	} else {
		// 如果没有设置PrivateKey字段，使用基于密码的身份验证。
		auth = append(auth, ssh.Password(c.Password)) // 将密码添加到auth切片中
	}

	// 拨号连接到SSH服务器
	c.Client, err = ssh.Dial("tcp", c.Addr+":"+c.Port, &ssh.ClientConfig{
		User:            c.User,
		Auth:            auth,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(), // 不安全，仅用于测试
	})
	if err != nil {
		return err
	}

	// 创建一个新的SSH会话
	session, err := c.Client.NewSession()
	if err != nil {
		return err
	}
	c.Session = session // 将创建的会话赋值给Session字段
	return nil
}

func (c *SshConfig) Run(cmd string) error {
	var b bytes.Buffer
	var d bytes.Buffer
	c.Session.Stdout = &b
	c.Session.Stderr = &d
	if err := c.Session.Run(cmd); err != nil {
		return errors.New(d.String())
	}
	return nil
}
